#include <iostream>
#include <string>

using namespace std;

/*
1. compute the sum of digits of a number:
input: 12345
1+2+3+4+5 = 15
output: 15

2. print the fibonacci numbers up to nth term

3. check a number whether it is prime or not
*/

void sumOfInputDigits() // 1
{
	int input;
	int counter = 0, sum = 0;
	cout << "Input numbers: ";
	cin >> input;

	while (input > counter)
	{
		counter = counter + 1;
		cout << counter;
		sum = sum + counter;

		if (counter == input)
		{
			cout << " = " << sum;
		}
		else
		{
			cout << " + ";
		}
	}
	cout << endl << endl;
}

void printFibonacci(int from) // 2
{
	static int firstNumber, position1 = 0, position2 = 1, next;

	if (from > 0)
	{
		next = position1 + position2;
		position1 = position2;
		position2 = next;

		cout << next << " ";
		printFibonacci(from - 1);
	}
}

/* Other code with for loop
int input, firstNumber, position1 = 0, position2 = 1, next;

	cout << "Input a number to output the series of Fibonacci: ";
	cin >> input;

	for (firstNumber = 0; firstNumber < input; firstNumber++)
	{
		if (firstNumber <= 1) {
			next = firstNumber;
		}
		else {
			next = position1 + position2;
			position1 = position2;
			position2 = next;
		}
		cout << next << " ";
	}
	cout << endl << endl;
*/

void checkIfPrime() // 3
{
	int inputNumber, i;
	bool isPrimeInput = true;

	cout << "\n\nEnter a number to check if it is a prime number or not: ";
	cin >> inputNumber;

	for (i = 2; i <= inputNumber / 2; i++)
	{
		if (inputNumber % i == 0)
		{
			isPrimeInput = false;
			break;
		}
	}
	if (isPrimeInput)
	{
		cout << inputNumber << " is a prime number";
	}
	else
	{
		cout << inputNumber << " is not a prime number";
	}
}

void main()
{
	sumOfInputDigits();
	cout << "Fibonacci: " << endl;
	printFibonacci(10);
	checkIfPrime();

	cout << endl;
	system("pause");
}
