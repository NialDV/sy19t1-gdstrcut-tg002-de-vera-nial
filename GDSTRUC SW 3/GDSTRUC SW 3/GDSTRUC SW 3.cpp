// GDSTRUC SW 3.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <conio.h>
using namespace std;

void main()
{
	string guildName;
	int numOfMem, arrayMembers[20];
	cout << "What is your Guild name? " << endl;
	cin >> guildName;

	cout << "How many members? " << endl;
	cin >> numOfMem;
	
	cout << "Input usernames: " << endl;
	for (int i = 0; i < numOfMem; i++)
	{
		cin >> arrayMembers[i];
	}
	
}


/*Create an application form for a GUILD that your users will use in your game :
1. Declare a name for the guild(user input)

2. Declare an initial size for the guild(user input)

3. Input the usernames of the initial members(user input)

4. Operations:
-Print every member
- Renaming a member
- Adding a member
- Deleting a member[EMPTY SLOT]

When a member is added or deleted, you are required to re - size the guild.
*/