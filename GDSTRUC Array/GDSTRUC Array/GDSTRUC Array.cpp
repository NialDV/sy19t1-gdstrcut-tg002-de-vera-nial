// GDSTRUC Array.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include <vector>
using namespace std;

void main()
{
	UnorderedArray<int> grades(5);
	for (int i = 0; i < 10; i++)
		grades.push(i);

	cout << "Initial values: " << endl;
	for (int i = 0; i < grades.getSize(); i++)
		cout << grades[i] << endl;

	cout << "\nRemove index 1" << endl;
	grades.remove(1);
		
	cout << "\nAfter removing index 1" << endl;
	for (int i = 0; i < grades.getSize(); i++)
		cout << grades[i] << endl;

	int userInput = 0;
	cout << "\nLinear Search: ";
	cin >> userInput;
	grades.linearSearch(userInput);
	
	
	system("pause");
};



