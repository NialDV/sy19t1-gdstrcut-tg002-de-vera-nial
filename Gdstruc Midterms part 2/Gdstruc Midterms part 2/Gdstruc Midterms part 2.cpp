#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>
#include <iostream>
#include <conio.h>
using namespace std;

void main()
{
	cout << "Enter size of array: ";
	int size;
	cin >> size;
	system("cls");

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	cout << "Generated array: " << endl;
	cout << "Unordered: ";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << "   ";
	cout << "\nOrdered: ";
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << "   ";

	int a = 0, decision = 0;

	while (a != 1) 
	{
		cout << "\n\nWhat do you want to do?" << endl;
		cout << "[1] remove element at index" << endl;
		cout << "[2] search for element" << endl;
		cout << "[3] expand and generate random values" << endl;
		cin  >> decision;

		if (decision == 1)
		{
			int index;
			cout << "\nEnter an index to remove: (index starts from 0 to ...)" << endl;
			cin >> index;
			cout << "Element/s in index [" << index << "] is removed." << endl;
			unordered.remove(index);
			ordered.remove(index);
			system("pause");
			system("cls");
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
				cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";
		}

		else if (decision == 2)//spacing would be nice
		{
			cout << "\nEnter number to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;
			system("pause");
			system("cls");
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";
		}

		else if (decision == 3)//Works, however once another operation is done
			//it doesn't expand properly, rather it returns to the original
			//array size
		{
			cout << "\nEnter size of array: ";
			int size;
			cin >> size;
			system("pause");
			system("cls");
			UnorderedArray<int> unordered(size);
			OrderedArray<int> ordered(size);

			for (int i = 0; i < size; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
				cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";
			
		}
	}
}

/*
What do you want to do?
1 - Remove element at index
2 - Search for element
3 - Expand and generate random values
*/
