// GDSTRUCT Finals.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <time.h>
#include <iostream>
#include <conio.h>
#include "Queue.h"
#include "Stack.h"
using namespace std;

//sources: GDSTRUC Midterms UnorderedArray.h 

int main()
{
	cout << "Enter size for element sets: ";
	int size;
	cin >> size;
	
	Queue<int> Queue(size);
	Stack<int> Stack(size);

	int decision = 0, a = 0;

	while (a != 1) // while loop won't end
	{
		cout << "What do you want to do? " << endl;
		cout << "[1] Push elements" << endl;
		cout << "[2] Pop elements" << endl;
		cout << "[3] Print everything then empty set" << endl;
		cin >> decision;

		if (decision == 1) // push elements
		{
			int index;
			cout << "Enter number: ";
			cin >> index;

			cout << "Top elements of sets: " << endl;
			Queue.push(index);
			Stack.push(index);

			cout << "Queue: " << Queue.top() << endl;
			cout << "Stack: " << Stack.top() << endl;
			system("pause");
			system("cls");
		}

		else if (decision == 2) // pop elements
		{
			cout << "Front elements popped." << endl;
			Queue.pop();
			Stack.popFront();

			cout << "Top elements of sets: " << endl;
			cout << "Queue: " << Queue.top() << endl;
			cout << "Stack: " << Stack.top() << endl;
			system("pause");
			system("cls");
		}

		else if (decision == 3) // print array then delete
		{
			cout << "Queue: \n";
			for (int i = 0; i < Queue.getSize(); i++)
				cout << Queue[i] << " " << endl;
			cout << "\nStack: \n";
			for (int i = Stack.getSize(); i > 0; i--)
				cout << Stack[i-1] << " " << endl;
			system("pause");
			system("cls");

			Queue.deleteArray();
			Stack.deleteArray();
		}
	}

}